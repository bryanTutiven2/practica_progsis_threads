CC = gcc
CFLAGS = -Wall -I .
LIB = -lpthread

.PHONY: all
all: client sha_server test

client: client.c csapp.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIB)

sha_server: sha_server.c csapp.o sha256.o daemon.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIB)

test: test.c csapp.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIB)

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c $^

sha256.o: sha256.c
	$(CC) $(CFLAGS) -c $^
daemon.o: daemon.c
	$(CC) $(CFLAGS) -c $^

.PHONY: clean
clean:
	rm -f *.o client sha_server test *~
