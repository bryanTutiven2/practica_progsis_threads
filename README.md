
# Pr�ctica 13: Uso de threads con Pthread

Este es un repositorio esqueleto para la pr�ctica 13 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.

### Uso ###
El repositorio contiene una aplicaci�n cliente - servidor. El servidor espera una cadena de caracteres del cliente, calcula el hash SHA256 de la misma y retorna el hash en binario al cliente.

Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 8080
```

Ejecutar el servidor:

```
./sha_server [-n] -p <port>
```

La opci�n -n es opcional y causa que el servidor ignore en el c�lculo del hash SHA256 el salto de l�nea '\n' al final de la cadena de caracteres enviada por el cliente. La opci�n -p especifica el puerto.

Ejemplo:

```
./sha_server -n -p 8080
```

```
./sha_server [-d] -p <port>
```

La opci�n -d es opcional, el servidor pasa a funcionar en segundo plano (daemon) debido a esto, para terminar con su ejecuci�n se puede usar el comando  "ps aux" lo que nos mostrara  en la pantalla los procesos que se estan ejecutando en nuestra computadora, luego se busca el id del "./sha_server" y se usa el comando kill -9 "idProceso" con lo cual se terminara la ejecuci�n del servidor en segundo plano. La opci�n -p especifica el puerto.

Ejemplo:
```
./sha_server -d -p 8080

```

./sha_server [-j] [value] -p <port>
```

La opci�n -j hace que el servidor use el patron de diseno productor consumidor, el valor que acompana a value define el numero de threads que se crearan para atender a los clientes del servidor, por lo cual no permitira la coneccion de nuevos clientes y se mantendran en espera. La opci�n -p especifica el puerto.

Ejemplo:
```
./sha_server -j 5 -p 8080


### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no esta) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y a�ada los nombres de los integrantes, luego borre esta l�nea.
# Integrantes
Bryan Tutiven
Juan Jimenez
