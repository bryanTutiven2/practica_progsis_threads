#include "csapp.h"
#include "sha256.h"


//Imprime cadena de bytes en formato hexadecimal
static void print_hex(const BYTE* data, size_t size)
{
    int i;
    for(i = 0; i < size; ++i)
        printf("%02x", data[i]);

    printf("\n");
}

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	BYTE sha256_buf[SHA256_BLOCK_SIZE];

	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(1);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);

	while (Fgets(buf, MAXLINE, stdin) != NULL) {
		Rio_writen(clientfd, buf, strlen(buf));
		Rio_readn(clientfd, sha256_buf, SHA256_BLOCK_SIZE);
		print_hex(sha256_buf, SHA256_BLOCK_SIZE);
	}
	Close(clientfd);
	exit(0);
}
