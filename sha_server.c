#include "csapp.h"
#include "sha256.h"
#include <getopt.h>
int num = 10;

void daemonize();
void *thread(void *vargp);
void hash(int connfd);

//Variables globales que determinan si una opción esta siendo usada
int nflag = 0; //Opción -n, ignora los saltos de línea
int pflag = 0; //Opción -p, especifica el puerto
int dflag = 0;
int jflag =0;

typedef struct {
int *buf;
int n;
int front;
int rear;
sem_t mutex;
sem_t slots;
sem_t items;
} sbuf_t;

sbuf_t sbuf;

void sbuf_init(sbuf_t *sp, int n);
void sbuf_deinit(sbuf_t *sp);
int sbuf_remove(sbuf_t *sp);
void sbuf_insert(sbuf_t *sp, int item);

int main(int argc, char **argv)
{
	int listenfd, connfdp;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	int c;
	pthread_t tid;
	

	while ((c = getopt (argc, argv, "ndjp:")) != -1)
	{
		switch(c)
		{
			case 'n':
				nflag = 1;
				break;
			case 'p':
				pflag = 1;
				port = optarg;
				break;
			case 'd':
				dflag = 1;
				break;
		    case 'j':
		    	jflag=1;
		    	num =   2 ; ///atoi(optarg);
		    	break;

			case '?':
			default:
				fprintf(stderr, "uso: %s [-n] [-d] [-j]  value -p <port>\n", argv[0]);
				return -1;
		}
	}

	//Opción -p es obligatoria
	if(!pflag)
	{
		fprintf(stderr, "uso: %s [-n] [-d] [-j] value -p <port>\n", argv[0]);
		return -1;
	}

	sbuf_init(&sbuf, num+5);

	listenfd = Open_listenfd(port);

	if(dflag == 1){
		
		printf("TRUE \n");
		daemonize();

	}
	
	//if(jflag ==1 ){
		for ( int i = 0; i < num; i++) /* Create worker threads */
			Pthread_create(&tid, NULL, thread, NULL);

	//}


	while (1) {

		clientlen = sizeof(clientaddr);
		//connfdp = Malloc(sizeof(int));
		connfdp = Accept(listenfd, (SA *) &clientaddr, &clientlen);
		if(jflag == 0)
			Pthread_create(&tid, NULL, thread, connfdp);

		if(jflag ==1){
			sbuf_insert(&sbuf, connfdp);
		}

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		//hash(connfd);
		//Close(connfd);
	}
	exit(0);
}



void *thread(void *vargp)
{
	Pthread_detach(pthread_self());
		while (1) {
		int connfd = sbuf_remove(&sbuf); /* Remove connfd from buffer */
        hash(connfd);
		//echo_cnt(connfd);
		/* Service client */
		Close(connfd);
	}

	//int connfd = *((int *)vargp);
	//Pthread_detach(pthread_self());
	//Free(vargp);
	//hash(connfd);
	//Close(connfd);
	//return NULL;
}

//Procesa los datos enviados por el cliente
void hash(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	BYTE sha256_buf[SHA256_BLOCK_SIZE];
	rio_t rio;
	SHA256_CTX ctx;

	
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("hashing: %s", buf);

		//Ignora '\n' asumiendo que esta al final de buf
		if(nflag)
			n--;

		//hash sha256
		sha256_init(&ctx);
		sha256_update(&ctx, (BYTE *) buf, n);
		sha256_final(&ctx, sha256_buf);

		Rio_writen(connfd, sha256_buf, SHA256_BLOCK_SIZE);
	}
}

void sbuf_init(sbuf_t *sp, int n)
{
		sp->buf = Calloc(n, sizeof(int));
		sp->n = n;
		/* Buffer holds max of n items */
		sp->front = sp->rear = 0;
		/* Empty buffer iff front == rear */
		Sem_init(&sp->mutex, 0, 1);
		/* Binary semaphore for locking */
		Sem_init(&sp->slots, 0, n);
		/* Initially, buf has n empty slots */
		Sem_init(&sp->items, 0, 0);
		/* Initially, buf has zero data items */
}

void sbuf_deinit(sbuf_t *sp)
{
Free(sp->buf);
}

void sbuf_insert(sbuf_t *sp, int item)
{
		P(&sp->slots);
		/* Wait for available slot */
		P(&sp->mutex);
		/* Lock the buffer */
		sp->buf[(++sp->rear)%(sp->n)] = item;
		/* Insert the item */
		V(&sp->mutex);
		/* Unlock the buffer */
		V(&sp->items);
		/* Announce available item */
}

int sbuf_remove(sbuf_t *sp)
{
		int item;
		P(&sp->items);
		/* Wait for available item */
		P(&sp->mutex);
		/* Lock the buffer */
		item = sp->buf[(++sp->front)%(sp->n)]; /* Remove the item */
		V(&sp->mutex);
		/* Unlock the buffer */
		V(&sp->slots);
		/* Announce available slot */
		return item;
}
